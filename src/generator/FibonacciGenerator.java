package generator;

import java.util.Scanner;

public class FibonacciGenerator {
	
	
	public boolean validateN(int n) {
		return n > 0;
	}
	
	public long calculateNthValue(int n) {
		long a = 1, b = 1, c = 0;
		for (int i = 2; i < n; i++) {
			c = a + b;
			a = b;
			b = c;
		}
		return b;
	}

	public static void main(String[] args) {
		Scanner userInput = new Scanner(System.in);
		FibonacciGenerator fib = new FibonacciGenerator();
		
		System.out.print("Enter a number: ");
		int n = userInput.nextInt();
		if (fib.validateN(n) == false) {
			System.out.println("Invalid number.");
			System.out.println("Enter an integer bigger than 2.");
		} else
			System.out.println(fib.calculateNthValue(n));
		
		userInput.close();
	}
}

